
import { _decorator, Component, Node, gfx, EffectAsset, Texture2D, Sprite, Material } from 'cc';
const { ccclass, property, executeInEditMode} = _decorator;
const { BlendFactor, CullMode} = gfx;
 
@ccclass('ExampleEffect')
@executeInEditMode
export class ExampleEffect extends Component {

    @property(EffectAsset)
    foo : EffectAsset = null!;

    @property(Texture2D)
    dissolveMap: Texture2D = null!;

    start () {
        const sprite = this.getComponent(Sprite);
        const mat = new Material();

        mat.initialize({
            effectAsset: this.foo,
            defines: {
                USE_TEXTURE: true,
            },
            states: {
                blendState: {
                    targets: [
                        {
                        blend: true,
                        blendSrc: BlendFactor.SRC_ALPHA,
                        blendDst: BlendFactor.ONE_MINUS_SRC_ALPHA,
                        blendDstAlpha: BlendFactor.ONE_MINUS_SRC_ALPHA
                        }
                    ]
                },
                rasterizerState: {
                    cullMode: CullMode.NONE,
                }
            }
        });

        sprite.customMaterial = mat;
        mat.setProperty('u_dissolveMap', this.dissolveMap!);
        mat.setProperty('dissolveThreshold', 0.1);

    }

}

