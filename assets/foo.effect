// Copyright (c) 2017-2020 Xiamen Yaji Software Co., Ltd.
CCEffect %{
  techniques:
  - passes:
    - vert: sprite-vs:vert
      frag: sprite-fs:frag
      depthStencilState:
        depthTest: false
        depthWrite: false
      blendState:
        targets:
        - blend: true
          blendSrc: src_alpha
          blendDst: one_minus_src_alpha
          blendDstAlpha: one_minus_src_alpha
      rasterizerState:
        cullMode: none
      properties:
        alphaThreshold: { value: 0.5 }
        u_dissolveMap: {value: White, editor: {tooltip: 'Noise map'}}
        dissolveThreshold: { value: 0.5, editor: {range: [0, 1, 0.01], slide: true, tooltip: 'dissolve threshold'}}
        u_dissolveColor: { editor: { type: color } }
        dissolveEdgeThreshold: {value: 0.05, editor: {range: [0, 1, 0.01], slide: true}}
        uvTilingMultiplier: {value: [1, 1], editor: {type: vec2}}
}%

CCProgram sprite-vs %{
  precision highp float;

  #include <cc-global>
  #if USE_LOCAL
    #include <cc-local>
  #endif
  #if SAMPLE_FROM_RT
    #include <common>
  #endif
  in vec3 a_position;
  in vec2 a_texCoord;
  in vec4 a_color;

  out vec4 color;
  out vec2 uv0;

  vec4 vert () {

    vec4 pos = vec4(a_position, 1);

    #if USE_LOCAL
      pos = cc_matWorld * pos;
    #endif

    #if USE_PIXEL_ALIGNMENT
      pos = cc_matView * pos;
      pos.xyz = floor(pos.xyz);
      pos = cc_matProj * pos;
    #else
      pos = cc_matViewProj * pos;
    #endif

    uv0 = a_texCoord;
    #if SAMPLE_FROM_RT
      CC_HANDLE_RT_SAMPLE_FLIP(uv0);
    #endif
    color = a_color;

    return pos;
  }
}%

CCProgram sprite-fs %{
  precision highp float;
  #include <embedded-alpha>
  #include <alpha-test>

  in vec4 color;

  uniform Dissolve {
    float dissolveThreshold;
    float dissolveEdgeThreshold;
  };

  uniform Tiling {
    vec2 uvTilingMultiplier;
  };

  uniform Color {
    vec4 u_dissolveColor;
  };

  #if USE_TEXTURE
    in vec2 uv0;
    uniform sampler2D u_dissolveMap;
    
    #pragma builtin(local)
    layout(set = 2, binding = 10) uniform sampler2D cc_spriteTexture;

  #endif

  vec4 frag () {
    vec4 o = vec4(1, 1, 1, 1);

    float value = 1.0;

    #if USE_TEXTURE

      vec4 dissolveMap = texture(u_dissolveMap, uv0 * uvTilingMultiplier);

      value *= dissolveMap.r;

    #endif

    if (value < dissolveThreshold){
      discard;
    }

    o *= texture(cc_spriteTexture, uv0);

    if (value <dissolveThreshold + 0.05){
      o = vec4(u_dissolveColor);
    }

    o *= color;

    return o;
  }
}%
