## Cocos Creator Shader Spike

Version: Cocos Creator 3.3.1


### About

Basic shader set up/experimentation based on the tutorial series here: 

https://discuss.cocos2d-x.org/t/tutorial-cocos-shader-series-blend-testing/55055


Just contains some static dissolve-style affects (with slightly more parameters than the tute)


### Notes
- Biggest blocker was `CLEANUP_IMAGE_CACHE` not being ticked on - without this, the UV coordinates will be broken in the build 
- Have to be careful with the texture import settings as well - you have to look at the inspector for the nested `texture` element to get wrapping options, for example